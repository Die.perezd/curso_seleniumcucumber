package runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import utils.LogsHelper;

import java.util.logging.Level;
import java.util.logging.Logger;


// inicializamos para que sea una clase ejecutable mediante cucumber
@RunWith(Cucumber.class)

// Indicamos donde buscará nuestros scenarios de pruebas o feature y cada uno de los pasos de nuestros scenarios
@CucumberOptions(
        features = "src/test/java/features",
        glue = {"stepsFiles"},
        plugin = {"json:reportes/report/cucumber_report.json"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)

public class testrunner {
    private static final Logger LOGGER = LogsHelper.getLogger();

    // Creamos un metodo para generar reportes en formato json
    @AfterClass
    public static void finish(){
        try {
            LOGGER.log(Level.INFO, "Generando reporte");
            String[] cmd = {"cmd.exe", "/c", "npm run report"};
            Runtime.getRuntime().exec(cmd);
            LOGGER.log(Level.INFO, "Reporte Generado Satisfactoriamente");
        }catch (Exception e){
            LOGGER.log(Level.WARNING, "No se pudo generar el reporte");
            e.printStackTrace();
        }
    }
}
