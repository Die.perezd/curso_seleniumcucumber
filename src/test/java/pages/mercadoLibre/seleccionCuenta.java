package pages.mercadoLibre;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.basePage;

public class seleccionCuenta extends basePage {

    // LOCALIZADORES DE LA PANTALLA DESPLEGADA
    private By btnSoyNuevo = By.xpath("//*[@id=\"registration-link\"]/span");


    public seleccionCuenta(WebDriver driver){
        super(driver);
    }

    public void clickSoyNuevo() throws Exception {
        this.click(btnSoyNuevo);
    }


}
