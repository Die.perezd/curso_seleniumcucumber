package pages.mercadoLibre;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.basePage;

public class homePage extends basePage {

    public homePage(WebDriver driver){
        super(driver);
    }

    // Localizadores de la pagina
    private String tituloPagina = "Mercado Libre Chile - Envíos Gratis en el día";
    private By textBoxBuscar = By.xpath("//*[@id=\"cb1-edit\"]");
    private By btnBuscar = By.xpath("/html/body/header/div/form/button");
    private By btnCookies = By.xpath("/html/body/div[2]/div[1]/div[2]/button[1]");

    public boolean homePageIsDisplayed() throws Exception {
        return this.getTitle().equals(tituloPagina);
    }

    public void clickOnTextBox() throws Exception {
        this.click(textBoxBuscar);
    }

    public void ingresarBusqueda(String texto) throws Exception {
        this.ingresarTexto(textBoxBuscar ,texto);
    }

    public void clickOnBuscar() throws Exception {
        this.click(btnCookies);
        this.click(btnBuscar);
    }


}
