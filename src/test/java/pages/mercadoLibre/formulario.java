package pages.mercadoLibre;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.basePage;

import static java.lang.Thread.sleep;

public class formulario extends basePage {
    // LOCALIZADORES DE LA PANTALLA DESPLEGADA
    private By ingresarNombre = By.xpath("//*[@id=\"firstName\"]");
    private By ingresarApellido = By.xpath("//*[@id=\"lastName\"]");
    private By ingresarClave = By.xpath("//*[@id=\"password\"]");
    private By btnAceptarTerminos = By.xpath("//*[@id=\"tyc_checkbox\"]");

    private By btnContinuar = By.xpath("//*[@id=\"root-app\"]/div/div/form/div[2]/button/span");

    private By texto = By.xpath("//*[@id=\"email-message\"]");


    String mensaje = "Completa este dato";

    public formulario(WebDriver driver){
        super(driver);
    }

    public void llenarFormulario( String nombre, String apellido, String clave) throws Exception {
        sleep(4000);
        this.ingresarTexto(ingresarNombre,nombre);
        this.ingresarTexto(ingresarApellido,apellido);
        this.ingresarTexto(ingresarClave,clave);
        this.click(btnAceptarTerminos);
        this.click(btnContinuar);
    }



    public void verificarMensaje() throws Exception {
        if (this.getText(texto).equals(mensaje)){
            System.out.println("Mensaje de error encontrado!: " + mensaje);
        } else{
            System.out.println("HUBO UN FALLO EN LAS PRUEBAS");
        }
    }
}
