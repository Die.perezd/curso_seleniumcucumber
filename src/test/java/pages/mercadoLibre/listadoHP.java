package pages.mercadoLibre;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.basePage;

import static java.lang.Thread.sleep;

public class listadoHP extends basePage {

    public listadoHP(WebDriver driver){
        super(driver);
    }

    // Localizadores de la pagina
    By tituloListado = By.xpath("/html/head/title");
    String titlePage = "Hp | MercadoLibre \uD83D\uDCE6";


    public By getTituloListado() {
        return tituloListado;
    }
    public String getTitlePage() {
        return titlePage;
    }

    public void clickPrimerElemento(String posicion) throws Exception {
        try {
            By seleccionElemento = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li["+posicion+"]/div/div/div[1]");

            this.click(seleccionElemento);
        } catch (Exception e){
            System.out.println(e);
        }
    }

}
