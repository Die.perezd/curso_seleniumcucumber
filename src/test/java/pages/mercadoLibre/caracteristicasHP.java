package pages.mercadoLibre;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.basePage;

public class caracteristicasHP extends basePage {

    // LOCALIZADORES DE LA PANTALLA DESPLEGADA
    private By btnComprar= By.xpath("//*[@id=\"buybox-form\"]/div[5]/div/button[1]");
    private By prueba = By.xpath("//button[@class='andes-button andes-button--loud']");

    public caracteristicasHP(WebDriver driver){
        super(driver);
    }

    // Ya localizado el elemento, llamamos el metodo click desde basePage para hacer click
    public void clickOnComprar() throws Exception {
        try {
            this.click(prueba);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
