package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Array;
import java.util.List;

public class basePage {

    private WebDriver driver;

    // creamos un constructor para inicializar el driver
    public basePage(WebDriver driver){
        this.driver = driver;
    }

    // creamos los metodos que utilizaremos el las demas paginas
    public void click(By element) throws Exception {
        try {
            driver.findElement(element).click();
        }catch (Exception e){
            throw new Exception("No se pudo clickear el elemento: "+ element);
        }
    }


    public String getTitle() throws Exception {
        try {
            return driver.getTitle();
        }catch (Exception e){
            throw new Exception("No se pudo obtener el titulo del driver");
        }
    }

    public String getText(By element) throws Exception {
        try {
            return driver.findElement(element).getText();
        }catch (Exception e){
            throw new Exception("No se pudo obtener el texto del driver");
        }
    }

    public void ingresarTexto(By element, String texto) throws Exception {
        try {
            driver.findElement(element).sendKeys(texto);
        }catch (Exception e){
            throw new Exception("No se pudo ingresar el texto de busqueda en el elemento: "+ element);
        }
    }


}
