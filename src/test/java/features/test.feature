Feature: Mercado libre
  Como usuario quiero buscar un computador marca HP en la pagina de mercado libre

  Scenario Outline: Comprando en mercado libre
    Given El usuario se encuentra en el home de mercado libre
    When Hace click en el textbox buscar
    And Escribe "<textoBusqueda>" en el buscador
    And Presiona el boton buscar
    And Presiona el elemento n "<posicion>" del listado
    And Presiona el boton Comprar ahora
    And Presiona el boton Soy nuevo
    And Ingresa los datos del formulario "<nombre>" "<apellido>" "<clave>"
    But No ingresa el correo
    Then El sistema arroja mensaje que faltan datos para crear cuenta

    Examples:
      | textoBusqueda | posicion   |  nombre  | apellido  | clave    |
      | hp            | 1          |  juan    | pavez     | jpavez123|
      | lenovo        | 2          |  claudio | perez     | cperez123|
      | televisor     | 1          |  pedro   | pepito    | pepito123|

