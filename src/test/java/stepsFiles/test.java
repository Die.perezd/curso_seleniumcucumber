package stepsFiles;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import stepsFiles.configuracionesGenerales.testBase;

import static java.lang.Thread.sleep;

public class test extends testBase {

    // Variables
    private String textoBusqueda;
    private String posicion;

    // creamos una variable de tipo chromedriver, utilizamos el metodo estatico creado en Hooks
    @Given("El usuario se encuentra en el home de mercado libre")
    public void elUsuarioSeEncuentraEnElHomeDeMercadoLibre() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertTrue(homePage.homePageIsDisplayed());
    }
    @When("Hace click en el textbox buscar")
    public void haceClickEnElTextboxBuscar() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        homePage.clickOnTextBox();
    }
    @When("Escribe {string} en el buscador")
    public void escribeEnElBuscador(String string) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        this.textoBusqueda = string;
        homePage.ingresarBusqueda(textoBusqueda);
    }
    @When("Presiona el boton buscar")
    public void presionaElBotonBuscar() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        homePage.clickOnBuscar();
    }
    @When("Presiona el elemento n {string} del listado")
    public void presionaElPrimerElementoDelListado(String valor) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        this.posicion = valor;
        listadoHP.clickPrimerElemento(posicion);
        // sleep(5000);
    }
    @When("Presiona el boton Comprar ahora")
    public void presionaElBotonComprarAhora() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        // sleep(2000);
        try {
            caracteristicasHP.clickOnComprar();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    @When("Presiona el boton Soy nuevo")
    public void presionaElBotonSoyNuevo() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        //sleep(2000);
        seleccionCuenta.clickSoyNuevo();
    }
    @When("Ingresa los datos del formulario {string} {string} {string}")
    public void ingresaLosDatosDelFormulario(String nombre, String apellido, String clave) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String nombreCliente = nombre;
        String apellidoCliente = apellido;
        String claveCliente = clave;
        formulario.llenarFormulario(nombreCliente, apellidoCliente, claveCliente);
    }
    @When("No ingresa el correo")
    public void noIngresaElCorreo() throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        sleep(2000);
    }
    @Then("El sistema arroja mensaje que faltan datos para crear cuenta")
    public void elSistemaArrojaMensajeQueFaltanDatosParaCrearCuenta() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        formulario.verificarMensaje();
    }


}
