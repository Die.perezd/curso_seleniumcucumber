package stepsFiles.configuracionesGenerales;

import org.openqa.selenium.chrome.ChromeDriver;
import pages.mercadoLibre.*;

public class testBase {

    protected ChromeDriver driver = Hooks.getDriver();
    protected homePage homePage = new homePage(driver);
    protected pages.mercadoLibre.caracteristicasHP caracteristicasHP = new caracteristicasHP(driver);
    protected pages.mercadoLibre.listadoHP listadoHP = new listadoHP(driver);
    protected seleccionCuenta seleccionCuenta = new seleccionCuenta(driver);
    protected pages.mercadoLibre.formulario formulario = new formulario(driver);
}
