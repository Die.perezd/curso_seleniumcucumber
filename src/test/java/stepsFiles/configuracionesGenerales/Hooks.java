package stepsFiles.configuracionesGenerales;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hooks {
    private static ChromeDriver driver;
    private static int numberOfCase = 0; // gracias a esto mostraremos en pantalla el numero de escenario que esta ejecutando
    // Este metodo se ejecutara primero que todos los metodos creados, asi inicializaremos el navegador
    @Before
    public void setUp(){
        numberOfCase++;
        System.out.println("Se esta ejecutando el escenario n°: " + numberOfCase );
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.mercadolibre.cl");
        driver.manage().window().maximize();
    }

    // Por su parte, este metodo es el ultimo en ejecutarse
    @After
    public void tearDown(Scenario scenario){
        if(scenario.isFailed()){
            byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png","ErrorScreenshot");
        }
        System.out.println("El escenario n°: " + numberOfCase + " se ejecutó correctamente");
        driver.quit();
    }

    // Con este metodo nuestra clase test utilizara el metodo que hemos creado
    public static ChromeDriver getDriver(){
        return driver;
    }

}
